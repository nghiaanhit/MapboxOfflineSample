﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace MapboxSample
{
    public class OfflineListRegionsDialog : AppCompatDialogFragment
    {
        public const string ITEMS = "ITEMS";

        public override Dialog OnCreateDialog(Bundle savedInstanceState)
        {
            Android.Support.V7.App.AlertDialog.Builder builder = new Android.Support.V7.App.AlertDialog.Builder(Activity);

            // Read args
            Bundle args = Arguments;
            List<string> offlineRegionsNames = (List<string>)(args == null ? null : args.GetStringArrayList(ITEMS));

            // I don't understand here
            List<string> items = offlineRegionsNames;

            builder.SetTitle("List of offline regions")
              .SetItems(1, (dialog, which) => Timber.Log.Timber.D("Selected item: %s", which.Which))
              .SetPositiveButton("Accept", (dialog, which) => Timber.Log.Timber.D("Dialog dismissed"));

            return builder.Create();
        }

        class DialogInterfaceOnClickListener : Java.Lang.Object, IDialogInterfaceOnClickListener
        {
            public Action<IDialogInterface, int> Clicked { get; set; }
            public DialogInterfaceOnClickListener(Action<IDialogInterface, int> Clicked)
            {
                this.Clicked = Clicked;
            }
            public void OnClick(IDialogInterface dialog, int which)
            {
                Clicked?.Invoke(dialog, which);
            }
        }
    }
}