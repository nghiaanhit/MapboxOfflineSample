﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MapboxSample
{
    public class OfflineDownloadRegionDialog : Android.Support.V7.App.AppCompatDialogFragment
    {
        public interface DownloadRegionDialogListener
        {
            void OnDownloadRegionDialogPositiveClick(string regionName);
        }

        DownloadRegionDialogListener listener;

        public override void OnAttach(Activity activity)
        {
            base.OnAttach(activity);
            listener = (DownloadRegionDialogListener)activity;
        }

        public override Dialog OnCreateDialog(Bundle savedInstanceState)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(Activity);
            EditText regionNameEdit = new EditText(Activity);
            builder.SetTitle("Choose a name for the region")
                .SetView(regionNameEdit)
                .SetPositiveButton("Start", new DialogInterfaceOnClickListener((dialog, which) =>
                {
                    string regionName = regionNameEdit.Text.ToString();
                    listener.OnDownloadRegionDialogPositiveClick(regionName);
                }));
            return builder.Create();
        }

        class DialogInterfaceOnClickListener : Java.Lang.Object, IDialogInterfaceOnClickListener
        {
            public Action<IDialogInterface, int> Clicked { get; set; }
            public DialogInterfaceOnClickListener(Action<IDialogInterface, int> Clicked)
            {
                this.Clicked = Clicked;
            }
            public void OnClick(IDialogInterface dialog, int which)
            {
                Clicked?.Invoke(dialog, which);
            }
        }
    }
}