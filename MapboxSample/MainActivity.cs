﻿using Android.App;
using Android.Widget;
using Android.OS;
using Com.Mapbox.Mapboxsdk.Maps;
using System;
using Com.Mapbox.Mapboxsdk.Constants;
using Com.Mapbox.Mapboxsdk.Offline;
using Com.Mapbox.Mapboxsdk;
using Com.Mapbox.Mapboxsdk.Camera;
using Android.Text;
using Com.Mapbox.Mapboxsdk.Geometry;
using static Com.Mapbox.Mapboxsdk.Offline.OfflineManager;
using static Com.Mapbox.Mapboxsdk.Offline.OfflineRegion;
using Android.Support.V7.App;
using System.Collections.Generic;

namespace MapboxSample
{
    [Activity(Label = "MapboxSample", MainLauncher = true, Theme = "@style/AppTheme")]
    public class MainActivity : AppCompatActivity, OfflineDownloadRegionDialog.DownloadRegionDialogListener
    {
        public const string ACCESS_TOKEN = "sk.eyJ1IjoibmF4YW10ZXN0IiwiYSI6ImNqNWtpb2d1ZzJpMngyd3J5ZnB2Y2JhYmQifQ.LEvGqQkAqM4MO3ZtGbQrdw";
        public const string JSON_CHARSET = "UTF-8";
        public const string JSON_FIELD_REGION_NAME = "FIELD_REGION_NAME";

        public const string STYLE_URL = Style.MapboxStreets;

        private MapView mapView;
        private MapboxMap mapboxMap;
        private ProgressBar progressBar;
        private Button downloadRegion;
        private Button listRegions;

        private bool isEndNotified;

        private OfflineManager offlineManager;
        private OfflineRegion offlineRegion;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Com.Mapbox.Mapboxsdk.Mapbox.GetInstance(this, ACCESS_TOKEN);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            bool connected = Mapbox.IsConnected().BooleanValue();
            System.Diagnostics.Debug.WriteLine("Mapbox is connected: " + connected);

            mapView = FindViewById<MapView>(Resource.Id.mapView);
            mapView.SetStyleUrl(STYLE_URL);
            mapView.OnCreate(savedInstanceState);
            mapView.GetMapAsync(new MapReadyCallback((mapboxMap) =>
            {
                System.Diagnostics.Debug.WriteLine("Map is ready");
                this.mapboxMap = mapboxMap;
                this.mapboxMap.StyleUrl = Style.Light;
            }));

            progressBar = FindViewById<ProgressBar>(Resource.Id.progress_bar);

            downloadRegion = FindViewById<Button>(Resource.Id.button_download_region);
            downloadRegion.Click += (s, e) =>
              {
                  handleDownloadRegion();
              };
            listRegions = FindViewById<Button>(Resource.Id.button_list_regions);
            listRegions.Click += (s, e) =>
              {
                  handleListRegions();
              };

            // Set up the OfflineManager
            offlineManager = OfflineManager.GetInstance(this);
        }

        protected override void OnStart()
        {
            base.OnStart();
            mapView.OnStart();
        }

        protected override void OnResume()
        {
            base.OnResume();
            mapView.OnResume();
        }

        protected override void OnPause()
        {
            base.OnPause();
            mapView.OnPause();
        }

        protected override void OnStop()
        {
            base.OnStop();
            mapView.OnStop();
        }

        protected override void OnSaveInstanceState(Bundle outState)
        {
            base.OnSaveInstanceState(outState);
            mapView.OnSaveInstanceState(outState);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            mapView.OnDestroy();
        }

        public override void OnLowMemory()
        {
            base.OnLowMemory();
            mapView.OnLowMemory();
        }

        private void handleDownloadRegion()
        {
            System.Diagnostics.Debug.WriteLine("handleDownloadRegion");

            // Show dialog
            OfflineDownloadRegionDialog offlineDownloadRegionDialog = new OfflineDownloadRegionDialog();
            offlineDownloadRegionDialog.Show(SupportFragmentManager, "download");
        }

        private void handleListRegions()
        {
            System.Diagnostics.Debug.WriteLine("handleListRegions");
            offlineManager.ListOfflineRegions(new ListOfflineRegionsCallback(
                (p0) =>
                {
                    Timber.Log.Timber.E("Error: %s", p0);
                },
                (offlineRegions) =>
                {
                    if (offlineRegions == null || offlineRegions.Length == 0)
                    {
                        Toast.MakeText(this, "You have no regions yet.", ToastLength.Short).Show();
                        return;
                    }

                    // Get regions info
                    List<string> offlineRegionsNames = new List<string>();
                    foreach (var offlineRegion in offlineRegions)
                    {
                        offlineRegionsNames.Add(OfflineUtils.convertRegionName(offlineRegion.GetMetadata()));
                    }

                    // Create args
                    Bundle args = new Bundle();
                    args.PutStringArrayList(OfflineListRegionsDialog.ITEMS, offlineRegionsNames);

                    // Show dialog
                    OfflineListRegionsDialog offlineListRegionsDialog = new OfflineListRegionsDialog();
                    offlineListRegionsDialog.Arguments = args;
                    offlineListRegionsDialog.Show(SupportFragmentManager, "list");
                }));
        }

        public void OnDownloadRegionDialogPositiveClick(string regionName)
        {
            this.RunOnUiThread(() =>
            {
                if (TextUtils.IsEmpty(regionName))
                {
                    Toast.MakeText(this, "Region name cannot be empty.", ToastLength.Short).Show();
                    return;
                }
                System.Diagnostics.Debug.WriteLine("Download started: " + regionName);
                startProgress();

                // Definition
                LatLngBounds bounds = mapboxMap.Projection.VisibleRegion.LatLngBounds;
                double minZoom = mapboxMap.CameraPosition.Zoom;
                double maxZoom = mapboxMap.MaxZoomLevel;
                float pixelRatio = this.Resources.DisplayMetrics.Density;
                OfflineTilePyramidRegionDefinition definition = new OfflineTilePyramidRegionDefinition(
                  STYLE_URL, bounds, minZoom, maxZoom, pixelRatio);

                // Sample way of encoding metadata from a JSONObject
                byte[] metadata = OfflineUtils.convertRegionName(regionName);


                // Create region
                offlineManager.CreateOfflineRegion(definition, metadata, new CreateOfflineRegionCallback((p0) =>
                {
                    System.Diagnostics.Debug.WriteLine("Offline region created: " + regionName);
                    this.offlineRegion = p0;
                    launchDownload();
                },
                (p0) =>
                {
                    Timber.Log.Timber.E("Error: %s", p0);
                }));
            });
            
        }

        private void launchDownload()
        {
            offlineRegion.SetObserver(new OfflineRegionObserver((p0) =>
            {
                Timber.Log.Timber.E("Mapbox tile count limit exceeded: %s", p0);
                offlineRegion.SetDownloadState(OfflineRegion.StateInactive);
            },
            (p0) =>
            {
                Timber.Log.Timber.E("onError: %s, %s", p0.Reason, p0.Message);
                offlineRegion.SetDownloadState(OfflineRegion.StateInactive);
            },
            (status) =>
            {
                double percentage = status.RequiredResourceCount >= 0
             ? (100.0 * status.CompletedResourceCount / status.RequiredResourceCount) :
             0.0;

                if (status.IsComplete)
                {
                    // Download complete
                    endProgress("Region downloaded successfully.");
                    offlineRegion.SetDownloadState(OfflineRegion.StateInactive);
                    offlineRegion.SetObserver(null);
                    return;
                }
                else if (status.IsRequiredResourceCountPrecise)
                {
                    // Switch to determinate state
                    setPercentage((int)Math.Round(percentage));
                }

                // Debug
                System.Diagnostics.Debug.WriteLine(status.CompletedResourceCount + status.RequiredResourceCount + " resources; " + status.CompletedResourceCount + " bytes downloaded.");
            }));
        }

        private void startProgress()
        {
            // Disable buttons
            downloadRegion.Enabled = false;
            listRegions.Enabled = false;

            // Start and show the progress bar
            isEndNotified = false;
            progressBar.Indeterminate = (true);
            progressBar.Visibility = Android.Views.ViewStates.Visible;
        }

        private void setPercentage(int percentage)
        {
            progressBar.Indeterminate = false;
            progressBar.SetProgress(percentage, false);
        }

        private void endProgress(string message)
        {
            // Don't notify more than once
            if (isEndNotified)
            {
                return;
            }

            // Enable buttons
            downloadRegion.Enabled = (true);
            listRegions.Enabled = (true);

            // Stop and hide the progress bar
            isEndNotified = true;
            progressBar.Indeterminate = false;
            progressBar.Visibility = Android.Views.ViewStates.Gone;

            // Show a toast
            Toast.MakeText(this, message, ToastLength.Short).Show();
        }

    }

    class MapReadyCallback : Java.Lang.Object, IOnMapReadyCallback
    {
        public Action<MapboxMap> mapReady { get; set; }
        public MapReadyCallback(Action<MapboxMap> mapReady)
        {
            this.mapReady = mapReady;
        }

        public void OnMapReady(MapboxMap p0)
        {
            mapReady?.Invoke(p0);
        }
    }


    class OfflineRegionObserver : Java.Lang.Object, IOfflineRegionObserver
    {
        public Action<long> MapboxTileCountLimitExceed { get; set; }
        public Action<OfflineRegionError> Error { get; set; }
        public Action<OfflineRegionStatus> StatusChanged { get; set; }
        public OfflineRegionObserver(Action<long> MapboxTileCountLimitExceed, Action<OfflineRegionError> Error, Action<OfflineRegionStatus> StatusChanged)
        {
            this.Error = Error;
            this.StatusChanged = StatusChanged;
            this.MapboxTileCountLimitExceed = MapboxTileCountLimitExceed;
        }
        public void MapboxTileCountLimitExceeded(long p0)
        {
            MapboxTileCountLimitExceed?.Invoke(p0);
        }

        public void OnError(OfflineRegionError p0)
        {
            Error?.Invoke(p0);
        }

        public void OnStatusChanged(OfflineRegionStatus p0)
        {
            StatusChanged?.Invoke(p0);
        }
    }
    class CreateOfflineRegionCallback : Java.Lang.Object, ICreateOfflineRegionCallback
    {
        public Action<OfflineRegion> Created { get; set; }
        public Action<string> Error { get; set; }
        public CreateOfflineRegionCallback(Action<OfflineRegion> Created, Action<string> Error)
        {
            this.Error = Error;
            this.Created = Created;
        }
        public void OnCreate(OfflineRegion p0)
        {
            Created?.Invoke(p0);
        }

        public void OnError(string p0)
        {
            Error?.Invoke(p0);
        }
    }

    class ListOfflineRegionsCallback : Java.Lang.Object, IListOfflineRegionsCallback
    {
        public Action<string> Error { get; set; }
        public Action<OfflineRegion[]> List { get; set; }
        public ListOfflineRegionsCallback(Action<string> Error, Action<OfflineRegion[]> List)
        {
            this.Error = Error;
            this.List = List;
        }
        public void OnError(string p0)
        {
            Error?.Invoke(p0);
        }

        public void OnList(OfflineRegion[] p0)
        {
            List?.Invoke(p0);
        }
    }
}

