﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using GoogleGson;

namespace MapboxSample
{
    public class OfflineUtils
    {
        public const string JSON_CHARSET = "UTF-8";
        public const string JSON_FIELD_REGION_NAME = "FIELD_REGION_NAME";

        public static string convertRegionName(byte[] metadata)
        {
            try
            {
                string json = metadata + JSON_CHARSET;
                JsonObject jsonObject = (JsonObject)new Gson().FromJson(json, Java.Lang.Class.FromType(typeof(JsonObject)));
                return jsonObject.Get(JSON_FIELD_REGION_NAME).AsString;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static byte[] convertRegionName(string regionName)
        {
            try
            {
                JsonObject jsonObject = new JsonObject();
                jsonObject.AddProperty(JSON_FIELD_REGION_NAME, regionName);
                return Encoding.ASCII.GetBytes(jsonObject.ToString().ToArray());
            }
            catch (Exception e)
            {
                Timber.Log.Timber.E(e.Message, "Failed to encode metadata: ");
            }

            return null;
        }
    }
}